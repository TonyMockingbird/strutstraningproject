package bo.impl;

import bo.UserBo;
import dao.UserDAO;
import entity.User;

public class UserBoImpl implements UserBo{
		
	UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO){
		this.userDAO = userDAO;
	}
	
	public void addUser(User user) {
		userDAO.addUser(user);
		
	}
	
	
	
}
