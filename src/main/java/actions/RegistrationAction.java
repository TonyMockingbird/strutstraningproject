package actions;


import bo.UserBo;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;



import entity.User;

public class RegistrationAction extends ActionSupport implements ModelDriven<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user = new User();
	UserBo userBo;
	
	public void setUserBo(UserBo userBo){
		this.userBo = userBo;
	}
	
	public Object getModel() {
		
		return user;
	}
	
	public String addUser() throws Exception{
		
			userBo.addUser(user);
		
		return "success";
	}
		
		
	public void validate() {
		 
				
		if(user.getPassword().length() > 0 && !(user.getPassword().equals("pass"))) {
			addFieldError("user.password", "The password is incorrect. Please, try again.");
		}
		if(!(user.getPassword().equals(user.getConfirmPassword()))){
			addFieldError("user.confirmPassword", "Passwords do not match");
		}
		
		if(user.getPassword().length() == 0){
			addFieldError("user.password", "Password required");
		}
			
		if(user.getFirstName().length() == 0){
			addFieldError("user.firstName", "User name required");
		}
		if(user.getLastName().length() == 0){
			addFieldError("user.lastName", "User last name required");
		}
		if(user.getEmail().length() == 0){
			addFieldError("user.email", "Email required");
		}
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
 
}