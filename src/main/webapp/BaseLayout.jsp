<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<!--
<style type="text/css">
body {
	font: 1em 'PT Sans', Arial, Helvetica;
	background: #EDEDEC;
	color: #333;
}

.container {
	width: 940px;
	margin: 0 auto;
}

.headers {
	background: #D1D1D0;
	height: 200px;
}
.menus{
	height: auto, 800px;
	float: left;
	width: 200px;
	height: 600px;
}

.bodys {
	height: 600px;
	background: #D1D1D0;
}
.registration{
	float: right;
	padding-top: 50px;
	padding-right: 50px;
}
.footers {
	background: #D1D1D0;
}
</style>
 -->
    <link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" />
</head>

<body>
	<div class="container">

		<div class="headers">
			<tiles:insertAttribute name="header" />
		</div>

		<div class="menus">
			<tiles:insertAttribute name="menu" />
		</div>

		<div class="bodys">
			<div class="registration">
					<tiles:insertAttribute name="body" />
			</div>
		</div>

		<div class="footers">
			<tiles:insertAttribute name="footer" />
		</div>

	</div>
</body>
</html>