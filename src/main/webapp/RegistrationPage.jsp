<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
   <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<title>Hello World</title>
	</head>

<body>
<h2>Registraion user</h2>

	<s:form action="registration" method="post">
		
		<%-- <s:textfield name="firstName" label="First name"/>  --%>
		<s:textfield name="user.firstName" key="global.firstname"/> 
		<s:textfield name="user.lastName" key="global.lastname"/>
		<s:select name="user.gender" list="{'male', 'female'}" key="global.gender"/>
		<s:textfield name="user.email" key="global.email"/> 
		<s:password name="user.password" key="global.password"/> 
		<s:password name="user.confirmPassword" key="global.confirmpassword"/> 
		
		<s:submit key="global.singup" align="center"/>
	
	</s:form>
	
	<s:url id="localeEN" namespace="/" action="locale">
		<s:param name="request_locale">en</s:param>
	</s:url>
	
	<s:url id="localeDE" namespace="/" action="locale">
		<s:param name="request_locale">de</s:param>
	</s:url>
	
	<s:a href="%{localeEN}">English</s:a>
	<s:a href="%{localeDE}">German</s:a>
	
	
</body>
</html>
